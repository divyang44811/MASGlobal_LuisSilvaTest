﻿/* Namespace javascript*/

// These kind of codding is related with the function scope, 
// I mean, in orden to have an encapsulation of which code is accesible and which not

var MyNameSpaceOrFunction = {

    MyFirstMethod: function(arg1, arg2)
    {
        return arg1 + arg2;
    }
    ,
    MySecondMethod: function(arg1, arg2)
    {
        return arg1 + arg2;
    }
};






//////////////////////
var GlobalTools = {
    /*Region para el DataTable de Jquery*/
    SetDataTable: function (selector) {
        $(selector).dataTable({
            "bJQueryUI": false,
            "aaSorting": [],
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todo"]],
            "oLanguage": { "sUrl": "../../js/Jquery.Datatables.Language.txt" },
            "sPaginationType": "full_numbers"
        });
    },

    /* Función que permite solo el ingreso de los caracteres que se especifican en la variable pattern cuando se llamana la Función desde el html
     Ejemplo onblur="AdmEcareTools.CheckValueRegExp(this,/\w/,'ig')"*/
    CheckValueRegExp: function (evt, pattern, flags) {
        var _RegExp = new RegExp(pattern, flags);
        var txtValue = $(evt).val();
        var _ArrayMatch = txtValue.match(_RegExp);
        var NewValue = "";
        if (_ArrayMatch != null) {
            NewValue = _ArrayMatch.join('');
            NewValue = NewValue.replace(/\s\s+/g, ' ');
        }
        $(evt).val(NewValue);
    },


    /*This section let validate on keydown just numbers */
    OnlyNumbers: function (selector) {
        $(selector).keydown(function (a) {
            var b = a.which ? a.which : event.keyCode;
            return b >= 48 && b <= 57 ||
                (b >= 96 && b <= 105 ||
                    (b >= 37 && b <= 40 ||
                        (13 == b || 8 == b || 46 == b)))
        });
    },


    /*Lineas para evitar carateres como: clic derecho y f12, ctrl shift y atl */
    AvoidRighClickAndF12Key: function () {

        $(document).keydown(function (event) {
            if (event.keyCode == 123) {
                return false;
            }
            else if (event.altKey & event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                return false;
            }
        });

        $(document).on("contextmenu", function (e) {
            e.preventDefault();
        });
    },


    /*Lineas para perimitir solo letras*/
    OnlyLetters: function (selector) {
        $(selector).keydown(function (e) {
            var tecla = (document.all) ? e.keyCode : e.which;

            //Validacion del teclado numer  derech y la tecla alt
            if ((tecla >= 96 && tecla <= 105) || e.altKey) { return false };

            if (tecla == 8) return true;
            if (tecla == 9) return true;
            if (tecla == 32) return true;
            if (tecla == 35) return true;
            if (tecla == 36) return true;
            if (tecla == 37) return true;
            if (tecla == 39) return true;
            if (tecla == 192) return true;
            if (e.ctrlKey && tecla == 86) { return true };
            if (e.ctrlKey && tecla == 67) { return true };
            if (e.ctrlKey && tecla == 88) { return true };


            var patron = /[a-zA-Z]/;
            te = String.fromCharCode(tecla);
            return patron.test(te);
        });
    },



    /*Lineas para perimitir solo letras y numeros*/
    OnlyAlphaNumerico: function (selector) {
        $(selector).keydown(function (e) {
            var tecla = (document.all) ? e.keyCode : e.which;


            //la tecla alt ctrl y shit
            if (e.altKey || e.ctrlKey || e.shiftKey || e.keyCode == 73) { return false };
            //Validacion del teclado numerico  derecho
            if ((tecla >= 96 && tecla <= 105)) { return true };
            //Validacion del teclado numerico  superior 
            if ((tecla >= 48 && tecla <= 57)) { return true };

            //Validacion del letras

            if (tecla == 8) return true;
            if (tecla == 9) return true;
            if (tecla == 32) return true;
            if (tecla == 35) return true;
            if (tecla == 36) return true;
            if (tecla == 37) return true;
            if (tecla == 39) return true;
            if (tecla == 192) return true;
            if (e.ctrlKey && tecla == 86) { return true };
            if (e.ctrlKey && tecla == 67) { return true };
            if (e.ctrlKey && tecla == 88) { return true };


            var patron = /[a-zA-Z]/;
            te = String.fromCharCode(tecla);
            return patron.test(te);

        });
    },

    /*Esta liena fue contruirda para evitar que el sistema reciba caracteres tales <Br> el cual arroja el error:
    A potentially dangerous Request.Form value was detected from the client
    Liena para limitar caracteres especiales como <>{}^*  186 226 222 */

    OnlyLettersSpecials: function (selector) {
        $(selector).keydown(function (e) {
            var tecla = (document.all) ? e.keyCode : e.which;
            /*Evitar  el copiado y pegado*/
            if (ctrlDown && (e.keyCode == vKey || e.keyCode == cKey)) return false;
            /*Evitar las teclas especiales*/
            if (tecla == 186 || tecla == 226 || tecla == 222) return false;
        });
    },

    //Metodo para abrir cualquier modal
    AbrirModal: function (selector) {
        $(selector).openModal();
    },

    //Metodo para cerrar cualquier modal
    CerrarModal: function (selector) {
        $(selector).closeModal();
    },

    //Metodo para cerrar cualquier modal
    CerrarModal: function (selector) {
        $(selector).closeModal();
    },


    //Metodo para abrir un modal de seew alert simple

    //Ejemplo: /*Lineas para validar teclas numericas */
    //"warning","success", "info","error"
    //    GlobalTools.CargarMensajeSA("¿Desea actualizar?",
    //                              "<p>Esta acción no podrá revertirse</p>",
    //                              "warning","SI","NO","Su petición se ha procesado de forma exitosa","NA");
    CargarMensajeSA: function (Titulo, TextHTML, TipoAlerta,
        TextBtnAceptar, TextBtnCancelar,
        MsgResultadoAceptar, MsgResultadoCancelar
    ) {
        swal({
            title: Titulo,
            text: TextHTML,
            html: true,
            type: TipoAlerta,
            showCancelButton: true,
            confirmButtonColor: '#d52b1e',
            cancelButtonColor: '#808080',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                swal({
                    title: "",
                    text: MsgResultadoAceptar,
                    html: true,
                    timer: 3000,
                    type: "success",
                    confirmButtonColor: "black"
                });
            } else {
                if (MsgResultadoCancelar.length > 0) {
                    swal({
                        title: "",
                        text: MsgResultadoCancelar,
                        html: true,
                        timer: 3000,
                        type: "info",
                        confirmButtonColor: "black"
                    });
                }
            }
        });
    },


    //Metodo para abrir un modal de seew alert simple

    //Ejemplo: /*Lineas para validar teclas numericas */
    //"warning","success", "info","error"
    //    GlobalTools.CargarMensajeSA("¿Desea actualizar?",
    //                              "<p>Esta acción no podrá revertirse</p>",
    //                              "warning","SI","NO","Su petición se ha procesado de forma exitosa","NA");
    CargarMensajeSA: function (Titulo, TextHTML, TipoAlerta,
        TextBtnAceptar, TextBtnCancelar,
        MsgResultadoAceptar, MsgResultadoCancelar
    ) {
        swal({
            title: Titulo,
            text: TextHTML,
            html: true,
            type: TipoAlerta,
            showCancelButton: true,
            confirmButtonColor: '#d52b1e',
            cancelButtonColor: '#808080',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                swal({
                    title: "",
                    text: MsgResultadoAceptar,
                    html: true,
                    timer: 3000,
                    type: "success",
                    confirmButtonColor: "black"
                });
            } else {
                if (MsgResultadoCancelar.length > 0) {
                    swal({
                        title: "",
                        text: MsgResultadoCancelar,
                        html: true,
                        timer: 3000,
                        type: "info",
                        confirmButtonColor: "black"
                    });
                }
            }
        });
    },

    //Metodo para abrir un modal de seew alert 2
    CargarMensajeSA_2_Simple: function (Titulo, TextHTML, TipoAlerta,
        TextBtnAceptar, TextBtnCancelar,
        MsgResultadoAceptar, MsgResultadoCancelar, Timer
    ) {
        swal({
            title: Titulo,
            text: MsgResultadoAceptar,
            type: TipoAlerta,
            timer: Timer,
            }
        )
    },

    //Metodo para abrir un modal de seew alert 2 simple con pregunta
    CargarMensajeSA_2_Pregunta: function (Titulo, TextHTML, TipoAlerta,
        TextBtnAceptar, TextBtnCancelar,
        MsgResultadoAceptar, MsgResultadoCancelar
    ) {
        swal({
            title: Titulo,
            text: TextHTML,
            type: TipoAlerta,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: TextBtnAceptar,
            cancelButtonText: TextBtnCancelar,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            this.ResultCargarMensajeSA_2_Pregunta = true;
        }, function (dismiss) {
            this.ResultCargarMensajeSA_2_Pregunta = true;
        });
    },
    ResultCargarMensajeSA_2_Pregunta : false,  // Varaible global 

};






