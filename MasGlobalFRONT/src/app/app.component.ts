import { Component } from '@angular/core';
import { LoaderService } from './services/loader/loader.service';
import { LoginComponent } from './modules/login/login.component';


// import '../assets/MaterializecssLocal/css/ghpages-materialize.css';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css'
  ]
})
export class AppComponent {
  showLoader: boolean;  // This means, if the loader should show it
  contenLoader: boolean;  // This means, if the content could loaded
  logOn: boolean;

  constructor(
      private loaderService: LoaderService,
      private LoginComponent: LoginComponent
    ) {
  }

  ngOnInit() {
      this.loaderService.statusLoading.subscribe((val: boolean) => {
          this.showLoader = val;
      });

      this.loaderService.statusContenido.subscribe((val: boolean) => {
        this.contenLoader = val;
      });

      this.loaderService.loginOn.subscribe((val: boolean) => {
        this.logOn = val;
      });

      // Security validation as a temporary way
      if (localStorage.getItem('UserAccess') === 'true') {
        this.logOn = true;
      }else {
        this.logOn = false;
      }

  }
}
