import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSidebarLeftComponent } from './nav-sidebar-left.component';

describe('NavSidebarLeftComponent', () => {
  let component: NavSidebarLeftComponent;
  let fixture: ComponentFixture<NavSidebarLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavSidebarLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavSidebarLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
