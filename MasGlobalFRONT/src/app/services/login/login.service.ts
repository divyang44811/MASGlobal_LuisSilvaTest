import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class LoginService {
  Model: any = {};
  headerss = new Headers({ 'Content-Type': 'application/json' });
  myParams = new URLSearchParams();

  UrlApi = 'http://luis-silva-temp3.azurewebsites.net/api/Login';
  // UrlApi = 'http://localhost:23321/api/Login';

  constructor(private http: Http) { }

  loginService(Model) {
    return this.http.post(this.UrlApi, JSON.stringify(Model), { headers: this.headerss }).map(result => {
        return result.json();
     });
  }
}
