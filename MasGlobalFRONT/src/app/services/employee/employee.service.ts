import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class EmployeeService {
  Model: any = {};
  headerss = new Headers({ 'Content-Type': 'application/json' });
  myParams = new URLSearchParams();

   UrlApi = 'http://luis-silva-temp3.azurewebsites.net/api/employee';
  // UrlApi = 'http://localhost:23321/api/employee';

  constructor(private http: Http) { }

  post(Model) {
    return this.http.post(this.UrlApi, JSON.stringify(Model), { headers: this.headerss }).map(result => {
        return result.json();
     });
  }

  getAll() {
    return this.http.get(this.UrlApi).map(result => {
      return result.json();
   });
  }

  delete(NameObj: string) {
    // this.myParams.append('id', idList );
      return this.http.delete(this.UrlApi + '?Name=' + NameObj, { headers: this.headerss }).map(result => {
        return result.json();
      });
  }

  public put(NameOld, Model) {
    return this.http.put(this.UrlApi + '?OdlId=' + NameOld, JSON.stringify(Model), { headers: this.headerss }).map(result => {
      return result.json();
    });
  }

}
