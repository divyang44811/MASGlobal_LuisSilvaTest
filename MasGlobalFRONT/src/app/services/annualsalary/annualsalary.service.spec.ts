import { TestBed, inject } from '@angular/core/testing';

import { AnnualSalaryService } from './annualsalary.service';

describe('AnnualSalaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnnualSalaryService]
    });
  });

  it('should be created', inject([AnnualSalaryService], (service: AnnualSalaryService) => {
    expect(service).toBeTruthy();
  }));
});
