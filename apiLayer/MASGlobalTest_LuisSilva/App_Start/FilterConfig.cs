﻿using System.Web;
using System.Web.Mvc;

namespace MASGlobalTest_LuisSilva
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
