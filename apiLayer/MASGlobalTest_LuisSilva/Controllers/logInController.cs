﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTOLayer;

namespace MASGlobalTest_LuisSilva.Controllers
{

    [RoutePrefix("api/Login")]
    public class logInController : ApiController
    {
        /// <summary>
        /// Credentials validate EXAMPLE: user: masglobal  pass:123456
        /// </summary>
        /// <param name="dto">dto for login</param>
        public bool Post([FromBody]DTOLoginAdmin dto)
        {
            if (dto.User == "masglobal" && dto.Pass == "123456")
                return true;
            return false;
        }
    }
}
