﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL;
using DTOLayer;

namespace MASGlobalTest_LuisSilva.Controllers
{
    public class employeeController : ApiController
    {

        employeeHander handler = new employeeHander();
        /// <summary>
        /// Get a list of employees
        /// </summary>
        /// <returns></returns>
        public List<DTOEmployee> Get()
        {
            return handler.get();
        }

        /// <summary>
        /// Get an employee by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DTOEmployee Get(string id)
        {
            return handler.get(id);
        }

        /// <summary>
        /// Post an employee
        /// </summary>
        /// <param name="dto">Data transfer from the client app</param>
        /// <returns>Return a string</returns>
        public string Post([FromBody]DTOEmployee dto)
        {
            return handler.post(dto);
        }

        /// <summary>
        /// Update an employee
        /// </summary>
        /// <param name="dto">Data transfer from the client app</param>
        /// <param name="oldId">Old Id</param>
        /// <returns>Return a string</returns>
        public string Put([FromBody]DTOEmployee dto, string oldId)
        {
            return handler.put(dto, oldId);
        }

        /// <summary>
        /// Remove an employee
        /// </summary>
        /// <param name="id"> Id</param>
        /// <returns>Return a string</returns>
        public string Delete(string id)
        {
            return handler.delete(id);
        }

    }
}
