﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesLayer;
using DTOLayer;

namespace BLL
{
    public class employeeHander
    {

        employeeRepo Repo = new employeeRepo();

        public List<DTOEmployee> get()
        {
            return Repo.get();
        }

        public DTOEmployee get(string id)
        {
            return Repo.get(id);
        }

        public string post(DTOEmployee dto)
        {
            return Repo.post(dto);
        }

        public string put(DTOEmployee dto, string oldId)
        {
            return Repo.put(dto, oldId);
        }

        public string delete(string id)
        {
            return Repo.delete(id);
        }


        public string getAnualSalary(string id)
        {
            return Repo.getAnualSalary(id);
           
        }
    }
}
