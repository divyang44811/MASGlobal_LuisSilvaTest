﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOLayer;

namespace AccesLayer
{
    public class employeeRepo
    {

        masGlobalEntitie db = new masGlobalEntitie();

        public List<DTOEmployee> get()
        {
            try
            {

                List<dbo_MASGLOBAL_employee> R = db.dbo_MASGLOBAL_employee.ToList();
                if (R.Count > 0)
                {
                    List<DTOEmployee> RR = new List<DTOEmployee>();
                    foreach (var item in R)
                    {
                        RR.Add(new DTOEmployee()
                        {
                            dateCreation = item.dateCreation,
                            fullName = item.fullName,
                            ID = item.ID,
                            rate = item.rate,
                            typeContract2 = item.typeContract
                        });
                    }
                    return RR;
                }
                return null;

            }
            catch (Exception Ex)
            {
                return null;
            }
        }

        public DTOEmployee get(string id)
        {
            try
            {
                dbo_MASGLOBAL_employee R = db.dbo_MASGLOBAL_employee.Where(t => t.ID.Equals(id)).FirstOrDefault();
                if (R != null)
                {

                    return new DTOEmployee()
                    {
                        dateCreation = R.dateCreation,
                        ID = R.ID,
                        fullName = R.fullName,
                        rate = R.rate,
                        typeContract2 = R.typeContract
                    };
                }
                return null;
            }
            catch (Exception Ex)
            {
                return null;
            }

        }

        public string post(DTOEmployee dto)
        {
            try
            {

                if (get(dto.ID) != null)
                    return "This ID exists already, please try with another one";

                db.dbo_MASGLOBAL_employee.Add(new dbo_MASGLOBAL_employee()
                {
                    dateCreation = DateTime.Now,
                    ID = dto.ID,
                    fullName = dto.fullName,
                    typeContract = dto.typeContract2,
                    rate = dto.rate
                });
                db.SaveChanges();
                return "Record added!";
            }
            catch (Exception Ex)
            {
                return null;
            }
        }

        public string put(DTOEmployee dto, string oldId)
        {
            try
            {
                dbo_MASGLOBAL_employee R = db.dbo_MASGLOBAL_employee.Where(t => t.ID.Equals(oldId)).FirstOrDefault();
                if (R != null)
                {
                    R.rate = dto.rate;
                    R.fullName = dto.fullName;
                    R.typeContract = dto.typeContract2;

                    db.SaveChanges();
                    return "Record updated!";
                }
                return "Record not found";
            }
            catch (Exception Ex)
            {
                return null;
            }
        }

        public string delete(string id)
        {
            try
            {
                dbo_MASGLOBAL_employee R = db.dbo_MASGLOBAL_employee.Where(t => t.ID.Equals(id)).FirstOrDefault();
                if (R != null)
                {
                    db.dbo_MASGLOBAL_employee.Remove(R);
                    db.SaveChanges();
                    return "Record removed!";
                }
                return "Record not found";
            }
            catch (Exception Ex)
            {
                return null;
            }
        }


        public string getAnualSalary(string id)
        {
            try
            {
                dbo_MASGLOBAL_employee R = db.dbo_MASGLOBAL_employee.Where(t => t.ID.Equals(id)).FirstOrDefault();
                if (R != null)
                {

                    int AnnualSalary = 0;
                    if (R.typeContract.StartsWith("Hourly"))
                    {
                        AnnualSalary = 120 * int.Parse(double.Parse(R.rate.ToString()).ToString()) * 12;
                    }
                    if (R.typeContract.StartsWith("Monthly"))
                    {
                        AnnualSalary = int.Parse(double.Parse(R.rate.ToString()).ToString()) * 12;
                    }
                    return AnnualSalary.ToString();
                }
                return "Record not found";
            }
            catch (Exception Ex)
            {
                return null;
            }
        }
    }
}
